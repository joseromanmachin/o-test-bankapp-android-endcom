package como.test.android.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovementsModel {
    @SerializedName("movimientos")
    @Expose
    private List<Movements> movimientos = null;

    public List<Movements> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<Movements> movimientos) {
        this.movimientos = movimientos;
    }
}
