package como.test.android.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BillModel {

    @SerializedName("cuenta")
    @Expose
    private List<Cuentum> cuenta = null;

    public List<Cuentum> getCuenta() {
        return cuenta;
    }

    public void setCuenta(List<Cuentum> cuenta) {
        this.cuenta = cuenta;
    }

    @Override
    public String toString() {
        return "BillModel{" +
                "cuenta=" + cuenta +
                '}';
    }
}


