package como.test.android.data.models;

public class CardDTO {
    private String numeroTarjeta;
    private String cuenta;
    private String issure;
    private String nombre;
    private String marca;
    private String status;
    private String saldo;
    private String tipoCuenta;

    public CardDTO() {
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getIssure() {
        return issure;
    }

    public void setIssure(String issure) {
        this.issure = issure;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    @Override
    public String toString() {
        return "CardDTO{" +
                "numeroTarjeta='" + numeroTarjeta + '\'' +
                ", cuenta='" + cuenta + '\'' +
                ", issure='" + issure + '\'' +
                ", nombre='" + nombre + '\'' +
                ", marca='" + marca + '\'' +
                ", status='" + status + '\'' +
                ", saldo='" + saldo + '\'' +
                ", tipoCuenta='" + tipoCuenta + '\'' +
                '}';
    }
}
