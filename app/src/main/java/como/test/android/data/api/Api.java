package como.test.android.data.api;

import como.test.android.data.models.BillModel;
import como.test.android.data.models.CardSaldosModel;
import como.test.android.data.models.CardsModel;
import como.test.android.data.models.MovementsModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    @GET("cuenta")
    Call<BillModel> getUser();

    @GET("saldos")
    Call<CardSaldosModel> getBalance();

    @GET("tarjetas")
    Call<CardsModel> getCards();

    @GET("movimientos")
    Call<MovementsModel> getMovements();
}
