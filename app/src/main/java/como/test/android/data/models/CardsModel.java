package como.test.android.data.models;

import java.util.List;

public class CardsModel {
    private List<Cards> tarjetas = null;

    public List<Cards> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(List<Cards> tarjetas) {
        this.tarjetas = tarjetas;
    }
}
