package como.test.android.ui.dashboard.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import como.test.android.data.api.Api;
import como.test.android.data.api.RetrofitInstance;
import como.test.android.data.models.BillModel;
import como.test.android.data.models.CardSaldosModel;
import como.test.android.data.models.CardsModel;
import como.test.android.data.models.Cuentum;
import como.test.android.data.models.MovementsModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DasboardViewModel extends ViewModel {

    Api retrofitInstance = RetrofitInstance.getInstance().create(Api.class);

    public MutableLiveData<Boolean> _loading = new MutableLiveData<>();
    public MutableLiveData<String> _error = new MutableLiveData<>();
    public MutableLiveData<Cuentum> cuentum = new MutableLiveData<>();
    public MutableLiveData<CardSaldosModel>  saldos = new MutableLiveData<>();
    public MutableLiveData<CardsModel> cards = new MutableLiveData<>();
    public MutableLiveData<MovementsModel> movemets = new MutableLiveData<>();

    public void getCuentum(){
        _loading.postValue(true);
        Call<BillModel> call = retrofitInstance.getUser();
        call.enqueue(new Callback<BillModel>() {
            @Override
            public void onResponse(Call<BillModel> call, Response<BillModel> response) {

                if (response.isSuccessful()){
                    cuentum.postValue(response.body().getCuenta().get(0));
                }else {
                    _error.postValue(response.message());
                }
            }

            @Override
            public void onFailure(Call<BillModel> call, Throwable t) {

                _error.postValue(t.getLocalizedMessage());
            }
        });
    }

    public void getBalances(){
        Call<CardSaldosModel> call = retrofitInstance.getBalance();
        call.enqueue(new Callback<CardSaldosModel>() {
            @Override
            public void onResponse(Call<CardSaldosModel> call, Response<CardSaldosModel> response) {
                if (response.isSuccessful()){
                    saldos.postValue(response.body());
                }else {
                    _error.postValue(response.message());
                }
            }

            @Override
            public void onFailure(Call<CardSaldosModel> call, Throwable t) {
                _error.postValue(t.getLocalizedMessage());
            }
        });
    }

    public void getCards() {
        Call<CardsModel> call = retrofitInstance.getCards();
        call.enqueue(new Callback<CardsModel>() {
            @Override
            public void onResponse(Call<CardsModel> call, Response<CardsModel> response) {
                if (response.isSuccessful()){
                    cards.postValue(response.body());
                }else{
                    _error.postValue(response.message());
                }
            }

            @Override
            public void onFailure(Call<CardsModel> call, Throwable t) {
                _error.postValue(t.getLocalizedMessage());
            }
        });
    }

    public void getMovements(){
        Call<MovementsModel> call = retrofitInstance.getMovements();
        call.enqueue(new Callback<MovementsModel>() {
            @Override
            public void onResponse(Call<MovementsModel> call, Response<MovementsModel> response) {
                if (response.isSuccessful()){
                    movemets.postValue(response.body());
                }else {
                    _error.postValue(response.message());
                }
            }

            @Override
            public void onFailure(Call<MovementsModel> call, Throwable t) {
                _error.postValue(t.getLocalizedMessage());
            }
        });
    }
}
