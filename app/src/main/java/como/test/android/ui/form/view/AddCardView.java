package como.test.android.ui.form.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;

import java.util.Objects;

import como.test.android.R;
import como.test.android.data.models.CardDTO;
import como.test.android.databinding.ActivityAddCardViewBinding;

public class AddCardView extends AppCompatActivity {

    ActivityAddCardViewBinding binding;
    CardDTO cardDTO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddCardViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardDTO = new CardDTO();
                builder.setTitle("JSON");
                cardDTO.setNumeroTarjeta(Objects.requireNonNull(binding.txtNumeroTarjeta.getText()).toString().trim());
                cardDTO.setCuenta(Objects.requireNonNull(binding.txtCuenta.getText()).toString().trim());
                cardDTO.setIssure(Objects.requireNonNull(binding.txtIssure.getText()).toString().trim());
                cardDTO.setNombre(Objects.requireNonNull(binding.txtNombreTarjeta.getText()).toString().trim());
                cardDTO.setMarca(Objects.requireNonNull(binding.txtMarca.getText()).toString().trim());
                cardDTO.setStatus(Objects.requireNonNull(binding.txtStatus.getText()).toString().trim());
                cardDTO.setSaldo(Objects.requireNonNull(binding.txtSaldo.getText()).toString().trim());
                cardDTO.setTipoCuenta(Objects.requireNonNull(binding.txtTipoCuenta.getText()).toString().trim());


                Gson gson = new Gson();
                builder.setMessage(gson.toJson(cardDTO));
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }
}