package como.test.android.ui.dashboard.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;

import como.test.android.R;
import como.test.android.databinding.ActivityDashboardViewBinding;
import como.test.android.ui.dashboard.adapter.AdapterBalances;
import como.test.android.ui.dashboard.adapter.AdapterCards;
import como.test.android.ui.dashboard.adapter.AdapterMovements;
import como.test.android.ui.dashboard.viewmodel.DasboardViewModel;
import como.test.android.ui.form.view.AddCardView;

public class DashboardView extends AppCompatActivity {

    private ActivityDashboardViewBinding binding;
    private  DasboardViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDashboardViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        viewModel = new ViewModelProvider(this).get(DasboardViewModel.class);

        viewModel.getCuentum();
        viewModel.getBalances();
        viewModel.getCards();
        viewModel.getMovements();
        observers();




        binding.textViewAddCard.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(),AddCardView.class);
            startActivity(intent);
        });





    }

    private void observers() {
        viewModel._error.observe(this, _error -> {

        });

        viewModel._loading.observe(this,_loading->{

        });

        viewModel.cuentum.observe(this,cuentum-> {
            binding.textViewName.setText(cuentum.getNombre());
            binding.textViewDate.setText(getString(R.string.last_session)+ " "+ cuentum.getUltimaSesion());

        });

        viewModel.saldos.observe(this,cardSaldosModel -> {

            AdapterBalances adapterBalances = new AdapterBalances(cardSaldosModel.getSaldos());
            binding.recyclerTotals.setAdapter(adapterBalances);
            binding.recyclerTotals.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        });

        viewModel.cards.observe(this,cardsModel -> {
            AdapterCards adapterCards = new AdapterCards(cardsModel.getTarjetas(),this);
            binding.recyclerCards.setAdapter(adapterCards);
            binding.recyclerCards.setLayoutManager(new LinearLayoutManager(this));
        });

        viewModel.movemets.observe(this,movementsModel -> {
            AdapterMovements adapterMovements = new AdapterMovements(movementsModel.getMovimientos(),this);
            binding.RecyclerViewMoves.setAdapter(adapterMovements);
            binding.RecyclerViewMoves.setLayoutManager(new LinearLayoutManager(this));
        });
    }
}