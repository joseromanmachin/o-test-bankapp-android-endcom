package como.test.android.ui.dashboard.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import como.test.android.data.models.Saldo;
import como.test.android.databinding.CardCustomTotalsBinding;


public class AdapterBalances extends RecyclerView.Adapter<AdapterBalances.ViewHolder> {

    public List<Saldo> saldosModels;
    CardCustomTotalsBinding binding;

    public AdapterBalances(List<Saldo> saldosModels){
        this.saldosModels = saldosModels;
    }
    @NonNull
    @Override
    public AdapterBalances.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(CardCustomTotalsBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterBalances.ViewHolder holder, int position) {
        binding.textViewText.setText("Saldo General $"+String.valueOf(saldosModels.get(position).getSaldoGeneral()));
        binding.textViewTotal.setText("Ingresos $"+String.valueOf(saldosModels.get(position).getIngresos()));

    }

    @Override
    public int getItemCount() {
        return saldosModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(@NonNull CardCustomTotalsBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }
}
