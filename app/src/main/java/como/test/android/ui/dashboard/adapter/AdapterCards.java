package como.test.android.ui.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import como.test.android.R;
import como.test.android.data.models.Cards;
import como.test.android.data.models.CardsModel;
import como.test.android.databinding.CardCustomListsCardsBinding;


public class AdapterCards extends RecyclerView.Adapter<AdapterCards.ViewHolder> {

    public List<Cards> cardsList;
    public Context context;
    CardCustomListsCardsBinding binding;

    public AdapterCards(List<Cards> cardsList, Context context){
        this.cardsList = cardsList;
        this.context = context;
    }
    @NonNull
    @Override
    public AdapterCards.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(CardCustomListsCardsBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCards.ViewHolder holder, int position) {
        binding.textViewActive.setText(cardsList.get(position).getEstado());
        binding.textViewNumCard.setText(cardsList.get(position).getTarjeta());
        binding.textViewName.setText(cardsList.get(position).getNombre());
        binding.textViewTotalMoney.setText(cardsList.get(position).getSaldo().toString());
        if (cardsList.get(position).getEstado().equals("desactivada")){
            binding.imageView.setImageDrawable(context.getDrawable(R.drawable.inactive_card));
        }
        binding.textViewHolder.setText(cardsList.get(position).getTipo());


    }

    @Override
    public int getItemCount() {
        return cardsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(@NonNull CardCustomListsCardsBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }
}
