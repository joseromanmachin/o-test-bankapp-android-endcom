package como.test.android.ui.dashboard.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import como.test.android.R;
import como.test.android.data.models.Movements;
import como.test.android.data.models.MovementsModel;
import como.test.android.databinding.CardCustomRecentMovesBinding;


public class AdapterMovements extends RecyclerView.Adapter<AdapterMovements.ViewHolder> {

    public List<Movements> movementsModels;
    public Context context;
    CardCustomRecentMovesBinding binding;

    public AdapterMovements(List<Movements> movementsModels, Context context){
        this.movementsModels = movementsModels;
        this.context = context;
    }
    @NonNull
    @Override
    public AdapterMovements.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(CardCustomRecentMovesBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull AdapterMovements.ViewHolder holder, int position) {
        binding.textViewTitle.setText(movementsModels.get(position).getDescripcion());
        binding.textViewTotal.setText(movementsModels.get(position).getMonto());
        binding.TexViewDate.setText(movementsModels.get(position).getFecha());
        if (movementsModels.get(position).getTipo().equals("abono")){
            binding.textViewTotal.setTextColor(context.getColor(R.color.green));
        }else if (movementsModels.get(position).getTipo().equals("cargo")){
            binding.textViewTotal.setTextColor(context.getColor(R.color.red));

        }

    }

    @Override
    public int getItemCount() {
        return movementsModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(@NonNull CardCustomRecentMovesBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }
}
